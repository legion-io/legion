# Legion Changelog

## v0.4.3
* Fixing bug with the Once actor where it was looking for use_runner instead of use_runner?
* Adding flag `use_database` for update task method inside helpers

## v0.4.2
* Upgrading dockerfile to use ruby 3
* Fixing issue with `Kernel.const_defined?` for TruffleRuby
* Updating legion-transport requirement to 1.1.9
* Updating the Dockerfile to use JIT flag

## v0.4.0
* Moving all legion-cli code to inside the legion gem instead of a seperate gem
* Removing bundled Legion::Data gem from LegionIO(must be installed with gem install legion-data)
* Removing JRuby support
* Removing the following gems from gemspec, hashdiff, legion-data, mysql2, sequel, bunny, bundler, codecov
* Skipping auto install if using bundler. It only works with the binary
* Adding new docker_deploy.rb script to automatically push a new docker tag with a deployment
* Adding multiple version dependencies to previously open ended gems
* Updating Dockerfile to have `--no-document --no-prerelease` flags
* Updating README
* Enabling rubocop-rspec
* Adding support to specify a LEX version to automatically install
* Updating `legion-transport` gem min version to 1.1.8

## v0.3.6
* Updating executables to use Legion::CLI
* Updating bitbucket-pipelines
* Changing base legion command to legionio and updating Dockerfile
* Blocking truffleruby from using Legion:Data as it's slow for unknown reasons
* Changing task update reverting to RMQ message to level debug

## v0.3.0
* Updating dependencies based on if we are using `JRUBY` or not
* Cleaning up some files and rubocop errors
* Adding new settings option for a lex definition with functionality to add it to the override settings stack
* Fixing supervision filename
* Cleaning up `Legion::Service`
* Cleaning up rogue comments
* Updating exe/legion to match bin/legion

## v0.1.1
Testing

## v0.1.0
Initial Release
