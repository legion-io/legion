# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'legion/version'

Gem::Specification.new do |spec|
  spec.name = 'legionio'
  spec.version       = Legion::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'Legion Core Software to string it all together'
  spec.description   = 'Legion Core runs Legion Framwork'
  spec.homepage      = 'https://bitbucket.org/legion-io/legion/'
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/legion-io/legion/src/master/'
  spec.metadata['documentation_uri'] = 'https://legionio.atlassian.net/wiki/spaces/LEGION/overview'
  spec.metadata['bug_tracker_uri'] = 'https://legionio.atlassian.net/browse/LEG'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 2.5.0'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'concurrent-ruby', '>= 1.1.7'
  spec.add_dependency 'concurrent-ruby-ext', '>= 1.1.7'
  spec.add_dependency 'daemons', '>= 1.3.1'
  spec.add_dependency 'oj', '>= 3.10'
  spec.add_dependency 'thor', '>= 1'

  spec.add_dependency 'legion-cache'
  spec.add_dependency 'legion-crypt', '>= 0.2.0'
  spec.add_dependency 'legion-exceptions'
  spec.add_dependency 'legion-json'
  spec.add_dependency 'legion-logging'
  spec.add_dependency 'legion-settings'
  spec.add_dependency 'legion-transport', '>= 1.1.9'

  spec.add_dependency 'lex-node'

  spec.add_development_dependency 'codecov'
  spec.add_development_dependency 'legion-data'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec_junit_formatter'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-rake'
  spec.add_development_dependency 'rubocop-rspec'
  spec.add_development_dependency 'simplecov'

  spec.add_development_dependency 'lex-conditioner'
  spec.add_development_dependency 'lex-health'
  spec.add_development_dependency 'lex-http'
  spec.add_development_dependency 'lex-lex'
  spec.add_development_dependency 'lex-log'
  spec.add_development_dependency 'lex-scheduler'
  spec.add_development_dependency 'lex-tasker'
  spec.add_development_dependency 'lex-transformer'
end
